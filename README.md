# Releases

- https://maheee-games.gitlab.io/minel-ve/ (only works in Chrome currently)


# Development

## Preparation

Copy win64 distribution of [LÖVE](https://love2d.org/) in folder _love-win64 (no subfolder).
Copy MacOS distribution of [LÖVE](https://love2d.org/) in folder _love-macos (no subfolder).

## How to Build

```./build_win.bat```
or
```./build_mac.bat```

## How to Run

```./start_win.bat```
or
```./start_mac.bat```

## How to open Debug Console

http://localhost:8000/
