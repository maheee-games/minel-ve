del /Q /S .\_build
del /Q /S .\_dist
mkdir _dist
mkdir _build
powershell Compress-Archive -Path .\src\* -CompressionLevel Fastest -DestinationPath .\_build\_build.zip
copy /b .\_love-win64\love.exe+.\_build\_build.zip .\_dist\_build.exe
copy .\_love-win64\*.dll .\_dist\
copy .\_love-win64\license.txt .\_dist\love_license.txt