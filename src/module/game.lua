local res = require("/module/resources")
local loc = require("/module/logic")
local cfg = require("/module/config")
local math = math
local string = string
local table = table
local love = love
module(...)

local state = {
  cursor = {fieldX = -1, fieldY = -1},
  fieldPos = 0,
  score = 0,
  lives = 5,
  speedUp = false,
  currentStageNo = 1,
  currentStage = cfg.stages[1],
  currentPause = 0,
  stopped = false,
  isEndlessGame = false
}

function setup(isEndlessGame, stage)
  state.fieldPos = 0
  state.score = 0
  state.lives = 5
  state.speedUp = false
  state.currentStageNo = stage or 1
  state.currentStage = cfg.stages[stage or 1]
  state.currentPause = 0
  state.stopped = false
  state.isEndlessGame = isEndlessGame
  res.playStart()

  if state.isEndlessGame then
    loc.resetRunning(state.currentStage.prop)
  else
    loc.resetStatic(state.currentStage.prop)
    state.fieldPos = 40
  end
end


function update(dt)
  if state.currentPause > 0 then
    -- reduce pause
    state.currentPause = state.currentPause - dt
  elseif not state.stopped and state.isEndlessGame then
    -- movement
    if state.speedUp then
      state.fieldPos = state.fieldPos + math.min(dt * 100, 2)
    else
      state.fieldPos = state.fieldPos + math.min(dt * 10, 2)
    end
    while state.fieldPos >= 30 do
      state.fieldPos = state.fieldPos - 30
      loc.progress(state.currentStage.prop)
    end
  end
  -- mouse handling
  local x, y = love.mouse.getPosition()
  state.cursor.fieldX, state.cursor.fieldY = mousePos2FieldPos(x, y)
end

function mousereleased(x, y, button)
  if state.stopped then
    return
  end
  state.cursor.fieldX, state.cursor.fieldY = mousePos2FieldPos(x, y)
  if state.cursor.fieldX > -1 and state.cursor.fieldY > -1 then
    if button == 1 then
      loc.openField(state.cursor.fieldX, state.cursor.fieldY, state.isEndlessGame)
    elseif button == 2 then
      loc.markField(state.cursor.fieldX, state.cursor.fieldY)
    elseif button == 3 then
      loc.surroundOpenField(state.cursor.fieldX, state.cursor.fieldY, state.isEndlessGame)
    end
  end
end

function keypressed(key, scancode, isrepeat)
  if key == "space" then
    state.speedUp = true
  end
end

function keyreleased(key, scancode, isrepeat)
  if key == "space" then
    state.speedUp = false
  end
end

function draw()
  function cx(x, m) return  50 + (x * 30) - m end
  function cy(y)    return 110 + (y * 30) end

  -- draw lives
  for i = 0, state.lives do
    res.drawMark(0, cx(i, -590), 70)
  end
  for i = state.lives, 5 do
    res.drawMark(1, cx(i, -590), 70)
  end

  local linesCnt = loc.endIndex - loc.startIndex
  -- draw field
  for y = 0, 15 do
    for x = 0, math.min(25, linesCnt) do
      local line = loc.lines[x + loc.startIndex]
      local field = line[y]
      local posX = cx(x, state.fieldPos)
      local posY = cy(y)

      -- res.drawMine(1, posX, posY)

      if field.open then
        if field.mine then
          res.drawMine(1, posX, posY)
        else
          res.drawNumber(field.neighborMines, posX, posY)
        end
      else
        if field.marked then
          res.drawMark(2, posX, posY)
        else
          res.drawEmpty(0, posX, posY)
        end
      end
    end
  end

  -- draw mouse cursor
  if not state.stopped and state.cursor.fieldX > -1 and state.cursor.fieldY > -1 then
    res.drawCursor(2, cx(state.cursor.fieldX - loc.startIndex, state.fieldPos), cy(state.cursor.fieldY))
  end

  -- draw resolver
  if state.isEndlessGame then
    for y = 0, 15 do
      if loc.resultLine[y].detected then
        res.drawMark(0, 10, cy(y))
      elseif loc.resultLine[y].exploded then
        res.drawMine(1, 10, cy(y))
      elseif loc.resultLine[y].faultyMarked then
        res.drawMark(1, 10, cy(y))
      else
        res.drawEmpty(1, 10, cy(y))
      end
    end
  end

  -- draw mask
  res.drawMask(state.isEndlessGame)

  -- draw scores
  local score = "" .. math.min(state.score, 999999)
  while string.len(score) < 6 do
    score = "0" .. score
  end
  love.graphics.setColor(0, 0, 0)
  love.graphics.setFont(res.fnt.reg)
  love.graphics.printf(score, 635, 30 + 2, 150, "right")
  love.graphics.printf("stg:" .. state.currentStageNo, 465, 30 + 2, 160, "right")
  if state.stopped then
    love.graphics.printf("stppd", 465, 70 + 2, 160, "right")
  elseif state.currentPause > 0 then
    love.graphics.printf("psd", 465, 70 + 2, 160, "right")
  else
    love.graphics.printf("rnng", 465, 70 + 2, 160, "right")
  end
  love.graphics.setColor(1, 1, 1)
end

function mousePos2FieldPos(x, y)
  if x > 10 and x < 790 then
    if y > 110 and y < 590 then
      x = x + state.fieldPos
      return
        math.floor((x -  50) / 30) + loc.startIndex,
        math.floor((y - 110) / 30)
    end
  end
  return -1, -1
end

function handleFault()
  state.currentPause = state.currentStage.pause
  state.lives = state.lives - 1
  if state.lives == 0 then
    state.stopped = true
  end
end

function handleDetected(no)
  state.score = state.score + no

  if state.score > state.currentStage.cleared and not state.currentStage.last then
    state.currentStageNo = state.currentStageNo + 1
    state.currentStage = cfg.stages[state.currentStageNo]
    res.playLvlUp()
  end
end

function loc.onMineExplode()
  res.playExplosion()
  handleFault()
end

function loc.onLineResult(detected, exploded, faultyMarked)
  if exploded > 0 then
    res.playExplosion()
    handleFault()
  elseif faultyMarked > 0 then
    res.playFaultyDetection()
    handleFault()
  elseif detected > 0 then
    handleDetected(detected)
    res.playSuccess()
  end
end
