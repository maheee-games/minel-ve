local math = math
module(...)

onLineResult = function (detected, exploded, faultyMarked) end
onMineExplode = function () end
resultLine = {}
lines = {}
startIndex = -1
endIndex = -1

local fieldsPerLine = 16

function resetRunning(mineProbability)
  resultLine = {}
  lines = {}
  startIndex = 0
  endIndex = -1

  for i = 0, 17 do
    addLine()
  end

  for i = 0, 14 do
    addLine()
    plantMines(endIndex - 1, mineProbability)
  end

  progress(mineProbability)
  progress(mineProbability)
  progress(mineProbability)
  progress(mineProbability)

  for y = 0, fieldsPerLine do
    openField(startIndex, y, true)
  end
end

function resetStatic(mineProbability)
  resultLine = {}
  lines = {}
  startIndex = 0
  endIndex = -1

  for i = 0, 25 do
    addLine()
  end
  for i = 1, 25 do
    plantMines(i, mineProbability)
  end

  for y = 0, fieldsPerLine do
    openField(startIndex, y, false)
  end
end

function createField()
  return {
    mine = false,
    neighborMines = 0,
    open = false,
    marked = false
  }
end

function createResultField(detected, exploded, faultyMarked)
  return {
    detected = detected,
    exploded = exploded,
    faultyMarked = faultyMarked
  }
end

function addLine()
  local line = {}
  for y = 0, fieldsPerLine - 1 do
    line[y] = createField(mineProbability)
  end
  endIndex = endIndex + 1
  lines[endIndex] = line
end

function plantMines(i, mineProbability)
  local before, current, after
  if i > startIndex then
    before = lines[i - 1]
  end
  current = lines[i]
  if i < endIndex then
    after = lines[i + 1]
  end

  for y = 0, fieldsPerLine - 1 do
    if math.random() < mineProbability then
      current[y].mine = true
      if before then
        before[y].neighborMines = before[y].neighborMines + 1
      end
      if after then
        after[y].neighborMines = after[y].neighborMines + 1
      end

      if y > 0 then
        current[y - 1].neighborMines = current[y - 1].neighborMines + 1
        if before then
          before[y - 1].neighborMines = before[y - 1].neighborMines + 1
        end
        if after then
          after[y - 1].neighborMines = after[y - 1].neighborMines + 1
        end
      end
      if y < fieldsPerLine - 1 then
        current[y + 1].neighborMines = current[y + 1].neighborMines + 1
        if before then
          before[y + 1].neighborMines = before[y + 1].neighborMines + 1
        end
        if after then
          after[y + 1].neighborMines = after[y + 1].neighborMines + 1
        end
      end
    end
  end
end

function progress(mineProbability)
  addLine()
  plantMines(endIndex - 1, mineProbability)

  resultLine = lines[startIndex]
  lines[startIndex] = nil
  startIndex = startIndex + 1

  -- keep opening fields if fields in second last line are open 
  for y = 0, fieldsPerLine - 1 do
    if lines[endIndex - 2][y].open and lines[endIndex - 2][y].neighborMines == 0 then
      openField(endIndex - 1, y, true)
    end
  end

  -- check result line
  local detected = 0
  local exploded = 0
  local faultyMarked = 0
  for y = 0, fieldsPerLine - 1 do
    local field = resultLine[y]
    if field.mine then
      if field.marked then
        resultLine[y] = createResultField(true, false, false)
        detected = detected + 1
      elseif not field.open then
        resultLine[y] = createResultField(false, true, false)
        exploded = exploded + 1
      end
    else
      if field.marked then
        resultLine[y] = createResultField(false, false, true)
        faultyMarked = faultyMarked + 1
      else
        resultLine[y] = createResultField(false, false, false)
      end
    end
  end
  onLineResult(detected, exploded, faultyMarked)
end

function markField(x, y)
  if not isInside(x, y) then
    return
  end
  if not lines[x][y].open then
    lines[x][y].marked = not lines[x][y].marked
  end
end

function openField(x, y, spareLast)
  if not isInside(x, y) then
    return
  end
  if spareLast and x >= endIndex then
    return
  end
  if lines[x][y].marked or lines[x][y].open then
    return
  end
  lines[x][y].open = true

  if lines[x][y].mine then
    onMineExplode()
  end

  -- open connected fields
  if lines[x][y].neighborMines == 0 then
    openField(x - 1, y - 1, spareLast)
    openField(x - 1, y, spareLast)
    openField(x - 1, y + 1, spareLast)
    openField(x, y - 1, spareLast)
    openField(x, y, spareLast)
    openField(x, y + 1, spareLast)
    openField(x + 1, y - 1, spareLast)
    openField(x + 1, y, spareLast)
    openField(x + 1, y + 1, spareLast)
  end
end

function isInside(x, y)
  return x >= startIndex and y >= 0 and x <= endIndex and y < fieldsPerLine
end

function hasMark(x, y)
  return isInside(x, y) and lines[x][y].marked
end

function isOpenMine(x, y)
  return isInside(x, y) and lines[x][y].open and lines[x][y].mine
end

function surroundOpenField(x, y, spareLast)
  if not isInside(x, y) then
    return
  end
  local field = lines[x][y]
  if not field.open or field.neighborMines == 0 then
    return
  end
  local surroundingMarks = 0
  for xp = -1, 1 do
    for yp = -1, 1 do
      if hasMark(x + xp, y + yp) or isOpenMine(x + xp, y + yp) then
        surroundingMarks = surroundingMarks + 1
      end
    end
  end
  if field.neighborMines == surroundingMarks then
    for xp = -1, 1 do
      for yp = -1, 1 do
        openField(x + xp, y + yp, spareLast)
      end
    end
  end
end
