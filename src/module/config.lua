module(...)

stages = {
  { cleared =   10, speed =  10, prop = 0.050, pause = 2 },
  { cleared =   50, speed =  15, prop = 0.050, pause = 2 },
  { cleared =  100, speed =  15, prop = 0.071, pause = 2 },
  { cleared =  150, speed =  20, prop = 0.071, pause = 2 },
  { cleared =  200, speed =  20, prop = 0.092, pause = 2 },
  { cleared =  250, speed =  25, prop = 0.092, pause = 2 },
  { cleared =  300, speed =  25, prop = 0.113, pause = 2 },
  { cleared =  350, speed =  30, prop = 0.113, pause = 2 },
  { cleared =  400, speed =  30, prop = 0.134, pause = 2 },
  { cleared =  450, speed =  35, prop = 0.155, pause = 2 },
  { cleared =  500, speed =  40, prop = 0.176, pause = 2 },
  { cleared =  550, speed =  45, prop = 0.197, pause = 2 },
  { cleared =  600, speed =  50, prop = 0.218, pause = 2 },
  { cleared =  650, speed =  55, prop = 0.239, pause = 2 },
  { cleared =  700, speed =  60, prop = 0.260, pause = 2 },
  { cleared =  750, speed =  65, prop = 0.281, pause = 2 },
  { cleared =  800, speed =  70, prop = 0.302, pause = 2 },
  { cleared = 1000, speed =  75, prop = 0.323, pause = 2 },
  { cleared = 2000, speed =  80, prop = 0.344, pause = 2 },
  { cleared = 3000, speed =  85, prop = 0.365, pause = 2 },
  { cleared = 4000, speed =  90, prop = 0.386, pause = 2 },
  { cleared = 5000, speed =  95, prop = 0.400, pause = 2, last = true }
}
