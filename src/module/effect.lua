local res = require("/module/resources")
local love = love
module(...)

local state = {
  mult = 80,
  progress = 0,
  from = 'empty0',
  to = 'empty1'
}

function setup(from, to)
  state.mult = 50
  state.progress = 0
  state.from = from
  state.to = to
end


function update(dt)
  state.progress = state.progress + dt * state.mult
end

function draw()
  function cx(x) return  10 + (x * 30) end
  function cy(y) return 110 + (y * 30) end
  function drawField(type, x, y)
    if type == 'empty0' then
      res.drawEmpty(0, x, y)
    elseif type == 'empty1' then
      res.drawEmpty(1, x, y)
    elseif type == 'mine0' then
      res.drawMine(0, x, y)
    elseif type == 'mine1' then
      res.drawMine(1, x, y)
    end
  end

  -- draw lives
  for i = 0, 5 do
    res.drawEmpty(0, cx(i) + 600, 70)
  end

  -- draw field
  for y = 0, 15 do
    for x = 0, 25 do
      drawField(
        x + y > state.progress and state.from or state.to,
        cx(x),
        cy(y))
    end
  end

  -- draw mask
  res.drawMask()
end

function isDone()
  return state.progress > 40
end


