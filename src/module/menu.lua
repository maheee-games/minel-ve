local res = require("/module/resources")
local loc = require("/module/logic")
local cfg = require("/module/config")
local love = love
local math = math
local ipairs = ipairs
module(...)

local state = {
  cursor = {fieldX = -1, fieldY = -1},
  startEndless = nil,
  startStage = nil
}

local buttons = {
  {x = 8, y = 2, event = function() state.startEndless = true end },
  {x = 8, y = 4, event = function() state.startStage = 1 end },
  {x = 8, y = 6, event = function() state.startStage = 5 end },
  {x = 8, y = 8, event = function() state.startStage = 10 end },
  {x = 8, y = 10, event = function() state.startStage = 15 end },
  {x = 8, y = 12, event = function() state.startStage = 20 end },
}

function setup()
  state.startEndless = nil
  state.startStage = nil
end

function update(dt)
  -- mouse handling
  local x, y = love.mouse.getPosition()
  state.cursor.fieldX, state.cursor.fieldY = mousePos2FieldPos(x, y)
end

function mousereleased(x, y, button)
  state.cursor.fieldX, state.cursor.fieldY = mousePos2FieldPos(x, y)
  if state.cursor.fieldX > -1 and state.cursor.fieldY > -1 then
    if button == 1 then
      for i,button in ipairs(buttons) do
        if button.x == state.cursor.fieldX and button.y == state.cursor.fieldY then
          button.event()
        end
      end
    end
  end
end

function draw()
  function cx(x) return  10 + (x * 30) end
  function cy(y) return 110 + (y * 30) end

  -- draw lives
  for i = 0, 5 do
    res.drawEmpty(0, cx(i) + 600, 70)
  end

  -- draw field
  for y = 0, 15 do
    for x = 0, 25 do
      local posX = cx(x)
      local posY = cy(y)

      res.drawEmpty(1, posX, posY)
    end
  end

  -- draw buttons
  for i,button in ipairs(buttons) do
    local x = cx(button.x)
    local y = cy(button.y)

    if i == 1 then
      res.drawMine(0, x - 30, y)
    else
      res.drawNumber(i - 1, x - 30, y)
    end
    res.drawEmpty(0, x, y)
  end

  -- draw mouse cursor
  if state.cursor.fieldX > -1 and state.cursor.fieldY > -1 then
    res.drawCursor(2, cx(state.cursor.fieldX), cy(state.cursor.fieldY))
  end

  -- draw mask
  res.drawMask()
end

function mousePos2FieldPos(x, y)
  if x > 10 and x < 790 then
    if y > 110 and y < 590 then
      return
        math.floor((x -  10) / 30),
        math.floor((y - 110) / 30)
    end
  end
  return -1, -1
end

function shouldStartEndless()
  return state.startEndless
end

function shouldStartStage()
  return state.startStage
end
