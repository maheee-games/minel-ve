local love = love
module(...)

img = {}
snd = {}
fnt = {}

function load()
  img.msk = love.graphics.newImage("/assets/mask.png")
  img.bar = love.graphics.newImage("/assets/maskbar.png")
  img.lgo = love.graphics.newImage("/assets/logo.png")
  img.ico = love.graphics.newImage("/assets/icons.png")
  fnt.reg = love.graphics.newFont("/assets/ErbosDraco1StOpenNbpRegular-l5wX.ttf", 24)
  fnt.inv = love.graphics.newFont("/assets/ErbosDraco1StNbpRegular-99V5.ttf", 60)
  fnt.invs = love.graphics.newFont("/assets/ErbosDraco1StNbpRegular-99V5.ttf", 48)

  img.qud = {}

  img.qud[0] = love.graphics.newQuad( 30,  0, 30, 30, 256, 256)
  img.qud[1] = love.graphics.newQuad( 60,  0, 30, 30, 256, 256)
  img.qud[2] = love.graphics.newQuad( 90,  0, 30, 30, 256, 256)
  img.qud[3] = love.graphics.newQuad(120,  0, 30, 30, 256, 256)
  img.qud[4] = love.graphics.newQuad(  0, 30, 30, 30, 256, 256)
  img.qud[5] = love.graphics.newQuad( 30, 30, 30, 30, 256, 256)
  img.qud[6] = love.graphics.newQuad( 60, 30, 30, 30, 256, 256)
  img.qud[7] = love.graphics.newQuad( 90, 30, 30, 30, 256, 256)
  img.qud[8] = love.graphics.newQuad(120, 30, 30, 30, 256, 256)

  img.qud.dark = love.graphics.newQuad(0, 0, 30, 30, 256, 256)

  img.qud.mark = {}
  img.qud.mark[0] = love.graphics.newQuad(  0, 60, 30, 30, 256, 256)
  img.qud.mark[1] = love.graphics.newQuad( 30, 60, 30, 30, 256, 256)
  img.qud.mark[2] = love.graphics.newQuad(120, 60, 30, 30, 256, 256)

  img.qud.mine = {}
  img.qud.mine[0] = love.graphics.newQuad( 60, 60, 30, 30, 256, 256)
  img.qud.mine[1] = love.graphics.newQuad( 90, 60, 30, 30, 256, 256)

  img.qud.cursor = {}
  img.qud.cursor[0] = love.graphics.newQuad( 0, 90, 30, 30, 256, 256)
  img.qud.cursor[1] = love.graphics.newQuad(30, 90, 30, 30, 256, 256)
  img.qud.cursor[2] = love.graphics.newQuad(60, 90, 30, 30, 256, 256)

  snd.blip = love.audio.newSource("/assets/blip.wav", "static")
  snd.explosion = love.audio.newSource("/assets/explosion.wav", "static")
  snd.hit = love.audio.newSource("/assets/hit.wav", "static")
  snd.ping = love.audio.newSource("/assets/ping.wav", "static")
  snd.powerup = love.audio.newSource("/assets/powerup.wav", "static")
end

function init()
  love.graphics.setFont(fnt.reg)
end

function drawMask(bar)
  love.graphics.draw(img.msk, 0, 0)
  if bar then
    love.graphics.draw(img.bar, 40, 107)
  end
  drawLogo()
end

function drawLogo()
  love.graphics.draw(img.lgo, 0, 0)
end

function drawEmpty(no, x, y)
  if no == 0 then
    love.graphics.draw(img.ico, img.qud.dark, x, y)
  else
    drawNumber(0, x, y)
  end
end

function drawNumber(no, x, y)
  love.graphics.draw(img.ico, img.qud[no], x, y)
end

function drawMark(no, x, y)
  love.graphics.draw(img.ico, img.qud.mark[no], x, y)
end

function drawMine(no, x, y)
  love.graphics.draw(img.ico, img.qud.mine[no], x, y)
end

function drawCursor(no, x, y)
  love.graphics.draw(img.ico, img.qud.cursor[no], x, y)
end

function playExplosion()
  love.audio.play(snd.explosion)
end

function playFaultyDetection()
  love.audio.play(snd.hit)
end

function playSuccess()
  love.audio.play(snd.ping)
end

function playLvlUp()
  love.audio.play(snd.powerup)
end

function playStart()
  love.audio.play(snd.blip)
end
