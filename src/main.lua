local lovebird = require("/vendor/lovebird")
require('/vendor/slam')

local res = require("/module/resources")
local gme = require("/module/game")
local eff = require("/module/effect")
local mnu = require("/module/menu")

function MenuState() return {type = 'menu'} end
function GameState(endless, stage) return {type = 'game', endless = endless, stage = stage} end
function TransitionState(from, to) return {type = 'tran', from = from, to = to, active = false} end

local state = MenuState()
local nextStates = {}

function startDefaultGame(stage)
  table.insert(nextStates, TransitionState('', 'empty0'))
  table.insert(nextStates, GameState(false, stage))
  table.insert(nextStates, TransitionState('empty0', ''))
end
function startEndlessGame()
  table.insert(nextStates, TransitionState('', 'empty0'))
  table.insert(nextStates, GameState(true))
  table.insert(nextStates, TransitionState('empty0', ''))
end
function startMenu()
  table.insert(nextStates, TransitionState('', 'empty0'))
  table.insert(nextStates, MenuState())
  table.insert(nextStates, TransitionState('empty0', ''))
end

function love.load()
  math.randomseed(os.time())
  res.load()
  res.init()

  gme.setup(false)
  eff.setup()
  mnu.setup()
end

function love.update(dt)
  -- lovebird.update()

  if table.getn(nextStates) == 0 then
    if state.type == 'game' then
      gme.update(dt)
    elseif state.type == 'menu' then
      mnu.update(dt)
      if mnu.shouldStartEndless() then
        startEndlessGame()
      else
        local sss = mnu.shouldStartStage()
        if sss then
          startDefaultGame(sss)
        end
      end
    end

  else
    if nextStates[1].type == 'tran' then
      if nextStates[1].active then
        if eff.isDone() then
          table.remove(nextStates, 1)
        end
      else
        nextStates[1].active = true
        eff.setup(nextStates[1].from, nextStates[1].to)
      end
    else
      state = table.remove(nextStates, 1)
      if state.type == 'game' then
        gme.setup(state.endless, state.stage)
      elseif state.type == 'menu' then
        mnu.setup()
      end
    end 
    eff.update(dt)
  end
end

function love.draw()
  if state.type == 'game' then
    gme.draw()
  elseif state.type == 'menu' then
    mnu.draw()
  end
  
  if table.getn(nextStates) > 0 then
    eff.draw()
  end
end

function love.mousereleased(x, y, button)
  if table.getn(nextStates) == 0 then
    if state.type == 'game' then
      gme.mousereleased(x, y, button)
    elseif state.type == 'menu' then
      mnu.mousereleased(x, y, button)
    end
  end
end

function love.keypressed(key, scancode, isrepeat)
  if table.getn(nextStates) == 0 then
    if state.type == 'game' then
      gme.keypressed(key, scancode, isrepeat)
    end
  end
end

function love.keyreleased(key, scancode, isrepeat)
  if table.getn(nextStates) == 0 then
    if state.type == 'game' then
      gme.keyreleased(key, scancode, isrepeat)
    end
  end

  if key == "escape" then
    startMenu()
  elseif key == "f2" then
    startEndlessGame()
  elseif key == "f3" then
    startDefaultGame(1)
  elseif key == "f4" then
    startDefaultGame(5)
  elseif key == "f5" then
    startDefaultGame(10)
  elseif key == "f6" then
    startDefaultGame(15)
  elseif key == "f7" then
    startDefaultGame(20)
  end
end
