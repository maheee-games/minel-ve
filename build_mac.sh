rm -R ./_build
rm -R ./_dist
mkdir _dist
mkdir _build
cd src
zip -r ../_build/_build.zip ./*
cd ..
cp -R ./_love-macos/love.app ./_dist/
rm -R ./_dist/love.app/Contents/_CodeSignature/
rm ./_dist/love.app/Contents/Info.plist
cp Info.plist ./_dist/love.app/Contents/
cp ./_build/_build.zip ./_dist/love.app/Contents/Resources/game.love
xattr -cr ./_dist/love.app
