# Fonts used
- ["Erbos Draco Monospaced NBP Font"](https://www.fontspace.com/total-fontgeek-dtf-ltd/erbos-draco-monospaced-nbp)  
Copyright (c) [total FontGeek DTF, Ltd.](http://totalfontgeek.blogspot.com/)

# Libraries used
- https://github.com/rxi/lovebird  
Copyright (c) 2017 rxi

- https://github.com/vrld/slam  
Copyright (c) 2011 Matthias Richter

# Build Win/Mac version
- https://github.com/love2d/love  
Copyright (c) 2006-2020 LOVE Development Team

# Build Web version
- https://github.com/Davidobot/love.js  
Copyright (c) 2016 Tanner Rogalsky
